// You will be supplied with two data files in CSV format.
// The first file contains statistics about various dinosaurs.
// The second file contains additional data.
//
// Given the following formula:
// speed = ((STRIDE_LENGTH / LEG_LENGTH) - 1) * SQRT(LEG_LENGTH * g)
// Where g = 9.8 m/s^2 (gravitational constant)
//
// Write a program to read in the data files from disk, it must then
// print the names of only the bipedal dinosaurs from fastest to slowest.
// Do not print any other information.

// Question is from an interactive coding interview with Oculus for a
// Build Engineer position, Aug 2017

// Steve's edit: I'm using vectors of strings rather than 
// CSV files so the entire work can be done in the program.
// I'm also printing the name AND the speed because it's
// easier to verify it's sorted correctly.

#include <cmath>
#include <string>
#include <sstream>
#include <vector>
#include "gtest/gtest.h"

std::vector<std::string> split(std::string str, char delimiter) {
    std::vector<std::string> tokens;
    std::stringstream ss(str); // Turn the string into a stream.
    std::string tok;

    while(getline(ss, tok, delimiter)) {
        tokens.push_back(tok);
    }

    return tokens;
}

TEST(split, EmptyString) {
    auto tokens = std::vector<std::string>{};
    EXPECT_EQ(tokens, split("", ','));
}

TEST(split, NoDelimiter) {
    auto tokens = std::vector<std::string>{"I am sam"};
    EXPECT_EQ(tokens, split("I am sam", ','));
}

TEST(split, OnlyDelimiter) {
    auto tokens = std::vector<std::string>{""};
    EXPECT_EQ(tokens, split(",", ','));
}

TEST(split, DelimiterAtBeginning) {
    auto tokens = std::vector<std::string>{"", "test"};
    EXPECT_EQ(tokens, split(",test", ','));
}

TEST(split, DelimiterAtEnd) {
    auto tokens = std::vector<std::string>{"test"};
    EXPECT_EQ(tokens, split("test,", ','));
}

TEST(split, MultipleDelimiters) {
    auto tokens = std::vector<std::string>{"", "", ""};
    EXPECT_EQ(tokens, split(",,,", ','));
}

TEST(split, Typical) {
    auto tokens = std::vector<std::string>{"Hadrosaurus", "1.2", "herbivore"};
    EXPECT_EQ(tokens, split("Hadrosaurus,1.2,herbivore", ','));
}

// Col 1: dinosaur name
// Col 2: leg length
// Col 3: herbivore/carnivore
std::vector<std::string> dataset1 = {
    "Hadrosaurus,1.2,herbivore",
    "Struthiomimus,0.92,omnivore",
    "Velociraptor,1.0,carnivore",
    "Triceratops,0.87,herbivore",
    "Euoplocephalus,1.6,herbivore",
    "Stegosaurus,1.40,herbivore",
    "Tyrannosaurus Rex,2.5,carnivore"
};

// Col 1: dinosaur name
// Col 2: stride length
// Col 3: quadrupedal/bipedal
std::vector<std::string> dataset2 = {
    "Euoplocephalus,1.87,quadrupedal",
    "Stegosaurus,1.90,quadrupedal",
    "Tyrannosaurus Rex,5.76,bipedal",
    "Hadrosaurus,1.4,bipedal",
    "Deinonychus,1.21,bipedal",
    "Struthiomimus,1.34,bipedal",
    "Velociraptor,2.72,bipedal"
};

float calculateSpeed(float strideLength, float legLength) {
    auto accelDueToGravity = float(9.81);
    return ((strideLength / legLength) - 1) * std::sqrt(legLength * accelDueToGravity);
}

int main(int argc, char** argv) {

    std::map<std::string, float> dinoStride;
    for (size_t row = 0; row < dataset2.size(); ++row) {
        std::vector<std::string> tokens = split(dataset2.at(row), ',');
        if (tokens.at(2) == "bipedal") {
            dinoStride[tokens.at(0)] = stof(tokens.at(1));
        }
    }

    std::map<std::string, float> dinoSpeed;
    for (size_t row = 0; row < dataset1.size(); ++row) {
        std::vector<std::string> tokens = split(dataset1.at(row), ',');
        if (dinoStride.find(tokens.at(0)) != dinoStride.end()) {
            dinoSpeed[tokens.at(0)] = calculateSpeed(dinoStride[tokens.at(0)], stof(tokens.at(1)));
        }
    }

    for (const auto &myPair : dinoSpeed) {
        std::cout << myPair.first << " " << myPair.second << "\n";
    }

    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
