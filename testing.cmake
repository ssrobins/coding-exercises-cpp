include_directories(${googletest_path}/googletest/include)
target_link_libraries(${project_name} gtest gtest_main)

add_test(NAME ${project_name} COMMAND ${project_name})

# Run unit tests after the build
add_custom_command(
    TARGET ${project_name}
    POST_BUILD
    COMMAND ctest -C $<CONFIGURATION> --output-on-failure
)