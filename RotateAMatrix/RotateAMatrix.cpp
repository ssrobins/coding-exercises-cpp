// Write a function to rotate a matrix 90 degrees clockwise
// Assume the matrix is square
// Rotate the matrix in place (no need to retain a copy of the original matrix)
// Source: https://www.youtube.com/watch?v=aClxtDcdpsQ

#include <vector>
#include "gtest/gtest.h"

bool matrixIsSquare(std::vector<std::vector<int>> const& matrix)
{
    bool matrixIsSquare = true;

    unsigned int firstDimension = matrix.size();
    for (unsigned int i = 0; i < firstDimension; ++i)
    {
        if (firstDimension != matrix.at(i).size())
        {
            matrixIsSquare = false;
            break;
        }
    }
    return matrixIsSquare;
}

bool rotateSquareMatrix(std::vector<std::vector<int>>& matrix)
{
    bool matrixIsRotated = false;
    
    unsigned int length = matrix.size();
    unsigned int maxIndex = length - 1;
    if (matrixIsSquare(matrix))
    {
        for (unsigned int layer = 0; layer < length / 2; ++layer)
        {
            unsigned int first = layer;
            unsigned int last = maxIndex - layer;
            for (unsigned int i = first; i < last; ++i)
            {
                unsigned int offset = maxIndex - i;

                // save top
                int top = matrix.at(first).at(i);

                // left -> top
                matrix.at(first).at(i) = matrix.at(offset).at(first);

                // bottom -> left
                matrix.at(offset).at(first) = matrix.at(last).at(offset);

                // right -> bottom
                matrix.at(last).at(offset) = matrix.at(i).at(last);

                // top -> right
                matrix.at(i).at(last) = top;
            }
        }
        matrixIsRotated = true;
    }

    return matrixIsRotated;
}

TEST(matrixIsSquare, Tests)
{
    EXPECT_TRUE(matrixIsSquare(std::vector<std::vector<int>>{}));
    EXPECT_FALSE(matrixIsSquare(std::vector<std::vector<int>>{{2}, {3, 0}}));
    EXPECT_FALSE(matrixIsSquare(std::vector<std::vector<int>>{{1, 2, 3}, {4, 5, 6}}));
}

TEST(rotateSquareMatrix, TwoByTwo)
{
    std::vector<std::vector<int>> matrix{
        {1, 2},
        {3, 4}
    };
    std::vector<std::vector<int>> rotatedMatrix{
        {3, 1},
        {4, 2}
    };
    EXPECT_TRUE(rotateSquareMatrix(matrix));
    EXPECT_EQ(rotatedMatrix, matrix);
}

TEST(rotateSquareMatrix, ThreeByThree)
{
    std::vector<std::vector<int>> matrix{
        {1, 2, 3},
        {4, 5, 6},
        {7, 8, 9}
    };
    std::vector<std::vector<int>> rotatedMatrix{
        {7, 4, 1},
        {8, 5, 2},
        {9, 6, 3}
    };
    EXPECT_TRUE(rotateSquareMatrix(matrix));
    EXPECT_EQ(rotatedMatrix, matrix);
}

TEST(rotateSquareMatrix, FourByFour)
{
    std::vector<std::vector<int>> matrix{
        { 1,  2,  3,  4},
        { 5,  6,  7,  8},
        { 9, 10, 11, 12},
        {13, 14, 15, 16}
    };
    std::vector<std::vector<int>> rotatedMatrix{
        {13,  9,  5,  1},
        {14, 10,  6,  2},
        {15, 11,  7,  3},
        {16, 12,  8,  4}
    };
    EXPECT_TRUE(rotateSquareMatrix(matrix));
    EXPECT_EQ(rotatedMatrix, matrix);
}

TEST(rotateSquareMatrix, FiveByFive)
{
    std::vector<std::vector<int>> matrix{
        { 1,  2,  3,  4,  5},
        { 6,  7,  8,  9, 10},
        {11, 12, 13, 14, 15},
        {16, 17, 18, 19, 20},
        {21, 22, 23, 24, 25},
    };
    std::vector<std::vector<int>> rotatedMatrix{
        {21, 16, 11,  6,  1},
        {22, 17, 12,  7,  2},
        {23, 18, 13,  8,  3},
        {24, 19, 14,  9,  4},
        {25, 20, 15, 10,  5},
    };
    EXPECT_TRUE(rotateSquareMatrix(matrix));
    EXPECT_EQ(rotatedMatrix, matrix);
}

int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}