// You have an array of integers, and for each index you want to find the product of every integer
// except the integer at that index.  Write a function get_products_of_all_ints_except_at_index()
// that takes an array of integers and returns an array of the products.
// For example, given:
//   [1, 7, 3, 4]
// your function would return:
//     [84, 12, 28, 21]
// by calculating:
//   [7 * 3 * 4, 1 * 3 * 4, 1 * 7 * 4, 1 * 7 * 3]
// Do not use division in your solution.
// Source: https://www.interviewcake.com/question/product-of-other-numbers

#include <iostream>
#include "gtest/gtest.h"
#include <vector>

std::vector<int> getProductsOfAllIntsExceptAtIndex(std::vector<int> numbers)
{
    std::vector<int> products(numbers.size(), 1);
    int product;
    int index;

    product = 1;
    index = numbers.size() - 1;
    while (index >= 0)
    {
        products.at(index) *= product;
        product *= numbers.at(index);
        --index;
    }

    product = 1;
    index = 0;
    while (index < static_cast<int>(numbers.size()))
    {
        products.at(index) *= product;
        product *= numbers.at(index);
        ++index;
    }

    return products;
}
/* // Alternate
std::vector<int> getProductsOfAllIntsExceptAtIndex(std::vector<int> numbers)
{
std::vector<int> products;
for (size_t productIndex = 0; productIndex < numbers.size(); ++productIndex)
{
    int product = 1;
    for (size_t numberIndex = 0; numberIndex < numbers.size(); ++numberIndex)
    {
        if (productIndex != numberIndex)
            product *= numbers.at(numberIndex);
    }
    products.push_back(product);
}
return products;
}*/

TEST(getProductsOfAllIntsExceptAtIndex, Empty)
{
    auto numberList = std::vector<int>{};
    auto productList = std::vector<int>{};
    EXPECT_EQ(productList, getProductsOfAllIntsExceptAtIndex(numberList));
}

TEST(getProductsOfAllIntsExceptAtIndex, AllZeros)
{
    auto numberList = std::vector<int>{0, 0};
    auto productList = std::vector<int>{0, 0};
    EXPECT_EQ(productList, getProductsOfAllIntsExceptAtIndex(numberList));
}

TEST(getProductsOfAllIntsExceptAtIndex, Example)
{
    auto numberList = std::vector<int>{1, 7, 3, 4};
    auto productList = std::vector<int>{84, 12, 28, 21};
    EXPECT_EQ(productList, getProductsOfAllIntsExceptAtIndex(numberList));
}

TEST(getProductsOfAllIntsExceptAtIndex, MyExample)
{
    auto numberList = std::vector<int>{2, 4, 6};
    auto productList = std::vector<int>{24, 12, 8};
    EXPECT_EQ(productList, getProductsOfAllIntsExceptAtIndex(numberList));
}

int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}