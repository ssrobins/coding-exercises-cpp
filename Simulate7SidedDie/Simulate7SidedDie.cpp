// You have a function rand5() that generates a random integer from 1 to 5.
// Use it to write a function rand7() that generates a random integer from 1 to 7.
// rand5() returns each integer with equal probability.
// rand7() must also return each integer with equal probability.
// Source: https://www.interviewcake.com/question/simulate-7-sided-die

#include <random>
#include <map>

std::default_random_engine generator;

// Output a random number within the range 1-5
int rand5()
{
    std::uniform_int_distribution<int> distribution(1, 5);
    return distribution(generator);
}

// Worst-case O(?) time (we might keep re-rolling forever) and O(1) space.
int rand7()
{
    while (true)
    {
        // When we call rand5() twice, we have 5*5=25 possible outcomes.
        int roll1 = rand5();
        int roll2 = rand5();

        // If each of our 7 possible results has an equal chance of occuring,
        // we'll need each outcome to occur in our set of possible outcomes the same number of times.
        // So our total number of possible outcomes must be divisible by 7.
        // 25 isn't evenly divisible by 7, but 21 is.
        // So when we get one of the last 4 possible outcomes, we throw it out and roll again.

        // Translate the result into a unique outcome_number in the range 1..25.
        int outcome_number = (roll1 - 1) * 5 + (roll2 - 1) + 1;

        // If the outcome_number is greater than 21, we throw it out and re - roll.
        // If not, we mod by 7 (and add 1).
        if (outcome_number > 21)
            continue;

        return outcome_number % 7 + 1;
    }
}

int main()
{
    int randomNumber = rand7();

    // Off-topic, but I want to check the 
    // uniformity of the distribution
    std::map <int, int> randomValueCount;
    for (int i = 0; i < 10000; ++i)
    {
        int randomNumber = rand7();
        randomValueCount[randomNumber] = ++randomValueCount[randomNumber];
    }

    return 0;
}