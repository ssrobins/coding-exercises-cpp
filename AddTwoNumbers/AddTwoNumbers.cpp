// Add two numbers without using arithmetic operators
// Source: Glassdoor interview reviews for Impinj
//         http://www.glassdoor.com/Interview/How-to-add-two-numbers-without-using-arithmetic-operators-QTN_479204.htm
// Answer from: http://stackoverflow.com/questions/4068033/add-two-integers-using-only-bitwise-operators
// Background info: https://en.wikipedia.org/wiki/Adder_%28electronics%29
// Boolean operations: http://www.cplusplus.com/doc/boolean/

#include "gtest/gtest.h"

unsigned int addTwoNumbers(unsigned int a, unsigned int b)
{
    unsigned int carry = a & b; // AND operator
    unsigned int result = a ^ b; // XOR operator
    while (carry != 0)
    {
        unsigned int shiftedcarry = carry << 1; // Left shift operator
        carry = result & shiftedcarry;
        result ^= shiftedcarry;
    }
    return result;
}

// Example
// a = 2 (decimal), 0010 (binary)
// b = 3 (decimal), 0011 (binary)
// carry = a & b = 0010 = 2
// result = a ^ b = 0001 = 1
// LOOP
//   shiftedcarry = 0100 = 4
//   carry = 0000 = 0
//   result = result ^ shiftedcarry = 0101 = 5

TEST(addTwoNumbers, Tests)
{
    EXPECT_EQ(0, addTwoNumbers(0, 0));
    EXPECT_EQ(1, addTwoNumbers(0, 1));
    EXPECT_EQ(0, addTwoNumbers(-1, 1));
    EXPECT_EQ(5, addTwoNumbers(3, 2));
    EXPECT_EQ(79, addTwoNumbers(56, 23));
}

int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}