// Write a function that reverses each word in a sentence
// Had a question like this at my interview at Placed

#include <string>
#include "gtest/gtest.h"

std::string reverseString(std::string const& word)
{
    std::string reverseWord = word;

    for (unsigned int letter = 0; letter < word.size()/2; ++letter)
    {
        int end = word.size() - 1;
        reverseWord.at(letter) = word.at(end - letter);
        reverseWord.at(end - letter) = word.at(letter);
    }

    return reverseWord;
}

/*
// Original version of the function that loops through the entire word
std::string reverseString(std::string const& word)
{
    std:: string reverseWord = word;

    for (unsigned int letter = 0; letter < word.size(); ++letter)
    {
        int end = word.size() - 1;
        reverseWord.at(letter) = word.at(end - letter);
    }

    return reverseWord;
}*/

std::string reverseLettersByWord(std::string const& sentence)
{
    std::string reverseWordsSentence;
    size_t startPos = 0;
    size_t endPos = 0;

    startPos = sentence.find_first_not_of(' ', startPos);

    while (startPos != std::string::npos)
    {
        endPos = sentence.find(' ', startPos);
        
        std::string word = sentence.substr(startPos, endPos - startPos);
        reverseWordsSentence.append(reverseString(word));

        startPos = sentence.find_first_not_of(' ', endPos);

        if (startPos != std::string::npos)
            reverseWordsSentence.append(" ");
        else
            break;

    }
    return reverseWordsSentence;
}

TEST(reverseString, Tests)
{
    EXPECT_EQ("", reverseString(""));
    EXPECT_EQ("a", reverseString("a"));
    EXPECT_EQ("ddo", reverseString("odd"));
    EXPECT_EQ("neve", reverseString("even"));
}

TEST(reverseLettersByWord, Tests)
{
    EXPECT_EQ("", reverseLettersByWord(""));
    EXPECT_EQ("a", reverseLettersByWord("a"));
    EXPECT_EQ("I ma !toorg", reverseLettersByWord("I am groot!"));
    EXPECT_EQ("elpitlum secaps", reverseLettersByWord("multiple  spaces"));
    EXPECT_EQ("gnidael", reverseLettersByWord("  leading"));
    EXPECT_EQ("gniliart", reverseLettersByWord("trailing  "));
}

int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}