// Asked this question during an interview at Ivy Softworks
//
// Not treating the number like a string, how do you find if an integer has a 7 in it.

#include <iostream>
#include "gtest/gtest.h"

bool isSevenInNumber(int number)
{
    number = abs(number);
    while (number != 0)
    {
        if (number % 10 == 7)
            return true;
        number = number / 10;
    }
    return false;
}

TEST(isSevenInNumber, Tests)
{
    EXPECT_FALSE(isSevenInNumber(0));
    EXPECT_FALSE(isSevenInNumber(666));
    EXPECT_TRUE(isSevenInNumber(756));
    EXPECT_TRUE(isSevenInNumber(576));
    EXPECT_TRUE(isSevenInNumber(567));
    EXPECT_TRUE(isSevenInNumber(777));
    EXPECT_TRUE(isSevenInNumber(-777));
}

int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}