// Write a function that takes a number n and returns an array containing a Fibonacci sequence of length n
// F(n) = F(n-1) + F(n-2) + ...
// First few values: 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, ...
// This was an example question sent ahead of an online coding assessment
// for a Solutions Engineering job at Facebook, July 2017

#include <map>
#include <vector>
#include "gtest/gtest.h"

// A simple solution (horribly inefficient):
//int fib(int n) {
//    if (n <= 1)
//        return 1;
//    return fib(n - 1) + fib(n - 2);
//}
//std::vector<int> fibArray(int n) {
//    std::vector<int> arr;
//    for (int x = 0; x <= n; ++x) {
//        arr.push_back(fib(x));
//    }
//    return arr;
//}
// Time Complexity: O(n * 2 ^ n)
// Space Complexity: O(n)

// So, how do we improve? Memorize the recursive function so we don't keep recomputing the same values:
//int fib(int n, std::map<int, int>& fibMemo) {
//    if (fibMemo.find(n) != fibMemo.end()) return fibMemo[n];
//    if (n <= 1) return 1;
//    const int result = fib(n - 1, fibMemo) + fib(n - 2, fibMemo);
//    fibMemo[n] = result;
//    return result;
//}
//std::vector<int> fibArray(int n) {
//    std::map<int, int> fibMemo;
//    std::vector<int> arr;
//    for (int x = 0; x <= n; ++x) {
//        arr.push_back(fib(x, fibMemo));
//    }
//    return arr;
//}
// Time Complexity: O(n)
// Space Complexity: O(n)
// Not bad, but the memorization is really overkill.

// We could write this cleaner by using the previous values as we build the array:
std::vector<int> fibArray(int n) {
    if (n < 0) return std::vector<int>{};
    if (n == 0) return std::vector<int>{1};
    if (n == 1) return std::vector<int>{1, 1};
    std::vector<int> arr = {1, 1};
    for (int x = 2; x <= n; ++x) {
        arr.push_back(arr[x - 1] + arr[x - 2]);
    }
    return arr;
}

TEST(fibSequence, NEqualsNegOne)
{
    auto fibSequence = std::vector<int>{};
    EXPECT_EQ(fibSequence, fibArray(-1));
}

TEST(fibSequence, NEqualsZero)
{
    auto fibSequence = std::vector<int>{1};
    EXPECT_EQ(fibSequence, fibArray(0));
}

TEST(fibSequence, NEqualsOne)
{
    auto fibSequence = std::vector<int>{1, 1};
    EXPECT_EQ(fibSequence, fibArray(1));
}

TEST(fibSequence, NEqualsTwo)
{
    auto fibSequence = std::vector<int>{1, 1, 2};
    EXPECT_EQ(fibSequence, fibArray(2));
}

TEST(fibSequence, NEqualsFour)
{
    auto fibSequence = std::vector<int>{1, 1, 2, 3, 5};
    EXPECT_EQ(fibSequence, fibArray(4));
}

int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}