// I like parentheticals(a lot).
// "Sometimes (when I nest them (my parentheticals) too much (like this (and this))) they get confusing."
//
// Write a function that, given a sentence like the one above, along with the position of an opening parenthesis, finds the corresponding closing parenthesis.
//
// Example: if the example string above is input with the number 10 (position of the first parenthesis), the output should be 79 (position of the last parenthesis).
// Source: https://www.interviewcake.com/question/matching-parens

#include <string>
#include "gtest/gtest.h"

int findClosingParen(std::string const& string, unsigned int openParenPosition)
{
    int closingParenPosition = -1;

    if (string.size() > openParenPosition &&
        string.at(openParenPosition) == '(')
    {
        unsigned int parenCounter = 1;
        for (unsigned int position = openParenPosition+1; position < string.size(); ++position)
        {
            if (string.at(position) == '(')
            {
                ++parenCounter;
            }
            else if (string.at(position) == ')')
            {
                --parenCounter;
                if (parenCounter == 0)
                {
                    closingParenPosition = position;
                    break;
                }
            }
        }
    }

    return closingParenPosition;
}

TEST(findClosingParen, Tests)
{
    EXPECT_EQ(-1, findClosingParen("", 0));
    EXPECT_EQ(-1, findClosingParen("", 1));
    EXPECT_EQ(5, findClosingParen("(test)", 0));
    EXPECT_EQ(79, findClosingParen("Sometimes (when I nest them (my parentheticals) too much (like this (and this))) they get confusing.", 10));
}

int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}