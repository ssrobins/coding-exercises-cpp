// You have a function rand7() that generates a random integer from 1 to 7.
// Use it to write a function rand5() that generates a random integer from 1 to 5.
// rand7() returns each integer with equal probability.
// rand5() must also return each integer with equal probability.
// Source: https://www.interviewcake.com/question/simulate-5-sided-die

#include <stdlib.h>
#include <time.h>
#include <map>
#include <random>

std::default_random_engine generator;

// Output a random number within the range 1-7
int rand7()
{
    std::uniform_int_distribution<int> distribution(1, 7);
    return distribution(generator);
}
/*
int rand7()
{
    // initialize random seed
    srand(time(NULL));

    return rand() % 7 + 1;
}*/

// Worst-case O(?) time (we might keep re-rolling forever) and O(1) space.
int rand5()
{
    int result = 7;

    // "re-roll" whenever we get a number greater than 5.
    while (result > 5)
    {
        result = rand7();
    }

    return result;
}

int main()
{
    int randomNumber = rand7();

    // Off-topic, but I want to check the 
    // uniformity of the distribution
    std::map <int, int> randomValueCount;
    for (int i = 0; i < 10000; ++i)
    {
        int randomNumber = rand5();
        randomValueCount[randomNumber] = ++randomValueCount[randomNumber];
    }

    return 0;
}