// Write a function to compute the parking cost over time based on the following:
//   First hour is free
//   $1 per 20 minute period after that
//   Max charge is $12
// Got this question at a technical interview at Akvelon.
// This was the actual parking cost at the facility!

#include "gtest/gtest.h"

// time is in minutes
unsigned int parkingCost(unsigned int time)
{
    unsigned int cost = 0;

    if (time > 60)
    {
        time -= 60;
        unsigned int timePerDollar = 20;
        cost = time / timePerDollar;
        if (time % timePerDollar != 0)
            ++cost;
    
        if (cost > 12)
            cost = 12;
    }

    return cost;
}

TEST(parkingCost, Tests)
{
    EXPECT_EQ(0, parkingCost(0));
    EXPECT_EQ(0, parkingCost(60));
    EXPECT_EQ(1, parkingCost(61));
    EXPECT_EQ(12, parkingCost(300));
    EXPECT_EQ(12, parkingCost(301));
}

int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}