﻿// Question and answer from Facebook Code Lab

// Write a function that takes an unsigned integer and returns the number of 1 bits it has.
//
// Example:
//
// The 32 - bit integer 11 has binary representation
//
// 00000000000000000000000000001011
// so the function should return 3.
//
// Note that since Java does not have unsigned int, use long for Java

// Hint:
//
// Bruteforce:
//
// Iterate 32 times, each time determining if the ith bit is a ’1′ or not.
// This is probably the easiest solution, and the interviewer would probably not be too happy about it.
// This solution is also machine dependent(You need to be sure that an unsigned integer is 32 - bit).
// In addition, this solution is not very efficient too because you need to iterate 32 times no matter what.
//
// A better solution :
//
// This special solution uses a trick which is normally used in bit manipulations.
// Notice what x - 1 does to bit representation of x.
// x - 1 would find the first set bit from the end, and then set it to 0, and set all the bits following it.
//
// Which means if x = 10101001010100
//                               ^
//                               |
//                               |
//                               |
//
// First set bit from the end
// Then x - 1 becomes 10101001010(011)
//
// All other bits in x - 1 remain unaffected.
// This means that if we do (x & (x - 1)), it would just unset the last set bit in x(which is why x&(x - 1) is 0 for powers of 2).

#include "gtest/gtest.h"
#include <bitset>
#include <iostream>

int numSetBits(unsigned int x) {
    unsigned int total_ones = 0;
    while (x != 0) {
        x = x & (x - 1);
        total_ones++;
    }
    return total_ones;
}

// & operator
// 1 if both bits are 1
// otherwise 0

// numSetBits(3)
// first loop
//   x = 3 is 0011 in binary
//   x - 1 = 2 is 0010 in binary
//   x = x & (x - 1)
//   = 0011 & 0010
//   = 0010 (2)
// second loop
//   x = 2 is 0010 in binary
//   x - 1 = 1 is 0011 in binary
//   x = x & (x - 1)
//     = 0010 & 0001
//     = 0000 (0)

TEST(numSetBits, Tests)
{
    EXPECT_EQ(0, numSetBits(0));
    EXPECT_EQ(1, numSetBits(1));
    EXPECT_EQ(1, numSetBits(2));
    EXPECT_EQ(2, numSetBits(3));
}

int main(int argc, char** argv)
{
    std::bitset<16> eleven(11);
    std::cout << "eleven: " << eleven << '\n';
    assert(numSetBits(11) == 3);

    std::bitset<16> sixtyeight(68);
    std::cout << "sixtyeight: " << sixtyeight << '\n';
    assert(numSetBits(68) == 2);

    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}