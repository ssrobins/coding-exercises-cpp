// I first experienced this question in an Amazon phone interview
//
// Write a program that prints the numbers from 1 to 100.
// But for multiples of three print �Fizz� instead of the number
// and for the multiples of five print �Buzz�.
// For numbers which are multiples of both three and five print �FizzBuzz�."
// Source: http://c2.com/cgi/wiki?FizzBuzzTest

#include <iostream>
#include <string>
#include "gtest/gtest.h"

std::string fizzBuzz(int number)
{
    std::string output;

    if (number % 3 == 0)
    {
        output += "Fizz";
    }
    if (number % 5 == 0)
    {
        output += "Buzz";
    }
    if (number % 3 != 0 && number % 5 != 0)
    {
        output += std::to_string(number);
    }

    return output;
}

TEST(fizzBuzz, Tests)
{
    EXPECT_EQ("Fizz", fizzBuzz(9));
    EXPECT_EQ("Buzz", fizzBuzz(10));
    EXPECT_EQ("FizzBuzz", fizzBuzz(15));
    EXPECT_EQ("17", fizzBuzz(17));
}

int main(int argc, char** argv)
{
    for (int number = 1; number <= 100; ++number)
    {
        std::cout << fizzBuzz(number) << std::endl;
    }

    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}