// Determine if the first given string is present in the second given string
// and if so, output its first position.
// This question was asked during the Build and Release Engineer interview at Motorola Solutions.

#include <string>
#include "gtest/gtest.h"

// Using range-based for loop

int findSubstringInString(std::string const& substring, std::string const& string)
{
    bool isMatch = false;
    int position = -1;

    if (substring.size() > 0)
    {
        for (unsigned int letter = 0; letter < string.size(); ++letter)
        {
            if (substring.at(0) == string.at(letter))
            {
                isMatch = true;
                position = letter;
                for (unsigned int match = 1; match < substring.size(); ++match)
                {
                    if (substring.at(match) != string.at(letter + match))
                    {
                        isMatch = false;
                        break;
                    }
                }
            }
        }
    }

    if (!isMatch)
        position = -1;

    return position;
}


// Original implementation

//int findSubstringInString(std::string const& substring, std::string const& string)
//{
//    bool isMatch = false;
//    int position = -1;
//
//    if (substring.size() > 0)
//    {
//        for (unsigned int letter = 0; letter < string.size(); ++letter)
//        {
//            if (substring.at(0) == string.at(letter))
//            {
//                isMatch = true;
//                position = letter;
//                for (unsigned int match = 1; match < substring.size(); ++match)
//                {
//                    if (substring.at(match) != string.at(letter + match))
//                    {
//                        isMatch = false;
//                        break;
//                    }
//                }
//            }
//        }
//    }
//
//    if (!isMatch)
//        position = -1;
//
//    return position;
//}

TEST(findSubstringInString, Tests)
{
    EXPECT_EQ(-1, findSubstringInString("", ""));
    EXPECT_EQ(-1, findSubstringInString("substring", ""));
    EXPECT_EQ(-1, findSubstringInString("", "string"));
    EXPECT_EQ(-1, findSubstringInString("bob", "I am Sam"));
    EXPECT_EQ(-1, findSubstringInString("tom", "Hi there"));
    EXPECT_EQ(9, findSubstringInString("tom", "Hi there tom"));
    EXPECT_EQ(5, findSubstringInString("Ra", "It's Raining"));
}

int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}