// Write a function that determines whether a number is a prime number
// http://stackoverflow.com/questions/1538644/c-determine-if-a-number-is-prime
//
// A prime number (or a prime) is a natural number greater than 1 that has no positive divisors other than 1 and itself
// Source: https://en.wikipedia.org/wiki/Prime_number

#include "gtest/gtest.h"

bool isPrimeNumber(unsigned int number)
{
    bool isPrime = false;

    if (number > 1)
    {
        isPrime = true;
        for (unsigned int n = 2; n <= number; ++n)
        {
            if (number % n == 0 && n != number)
            {
                isPrime = false;
                break;
            }
        }
    }

    return isPrime;
}

TEST(isPrimeNumber, Tests)
{
    EXPECT_FALSE(isPrimeNumber(0));
    EXPECT_FALSE(isPrimeNumber(1));
    EXPECT_TRUE(isPrimeNumber(2));
    EXPECT_TRUE(isPrimeNumber(3));
    EXPECT_FALSE(isPrimeNumber(4));
    EXPECT_TRUE(isPrimeNumber(5));
    EXPECT_FALSE(isPrimeNumber(6));
    EXPECT_TRUE(isPrimeNumber(7));
    EXPECT_FALSE(isPrimeNumber(8));
    EXPECT_FALSE(isPrimeNumber(9));
    EXPECT_FALSE(isPrimeNumber(10));
}

int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}