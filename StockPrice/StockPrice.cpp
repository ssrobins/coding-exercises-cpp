// Very similar to a question I was asked at an Amazon in-person interview
//
// Suppose we could access yesterday's stock prices as a list, where:
//
// The indices are the time in minutes past trade opening time, which was 9:30am local time.
// The values are the price in dollars of Apple stock at that time.
// So if the stock cost $500 at 10:30am, stock_prices_yesterday[60] = 500.
//
// Write an efficient function that takes stock_prices_yesterday and returns the best profit I could have made from 1 purchase and 1 sale of 1 Apple stock yesterday.
//
//For example :
//
// stock_prices_yesterday = [10, 7, 5, 8, 11, 9]
//
// get_max_profit(stock_prices_yesterday)
// # returns 6 (buying for $5 and selling for $11)
//
// No "shorting"�you must buy before you sell.You may not buy and sell in the same time step(at least 1 minute must pass).
// Source: https://www.interviewcake.com/question/stock-price

#include <vector>
#include "gtest/gtest.h"
#include <algorithm>

int getMaxProfit(std::vector<int> const& stockPricesOverTime)
{
    int maxProfit = 0;

    // Must have at least two prices to compare the profit
    if (stockPricesOverTime.size() > 1)
    {
        // Initialize minPrice and maxProfit to the first price and the first possible profit
        int minPrice = stockPricesOverTime.at(0);
        maxProfit = stockPricesOverTime.at(1) - stockPricesOverTime.at(0);

        for (const auto& currentPrice : stockPricesOverTime)
        {
            if (currentPrice == stockPricesOverTime.front())
                continue;

            int potentialProfit = currentPrice - minPrice;

            maxProfit = std::max(maxProfit, potentialProfit);

            minPrice = std::min(minPrice, currentPrice);
        }

        // Old-school loop, but no if statement and continue required
        /*
        for (unsigned int time = 1; time < stockPricesOverTime.size(); ++time)
        {
            int currentPrice = stockPricesOverTime.at(time);

            int potentialProfit = currentPrice - minPrice;

            maxProfit = std::max(maxProfit, potentialProfit);

            minPrice = std::min(minPrice, currentPrice);
        }*/
    }
    return maxProfit;
}

TEST(getMaxProfit, Tests)
{
    // No stock prices means 0 max profit.  Can't make/lose money if there is no stock to buy/sell
    EXPECT_EQ(0, getMaxProfit(std::vector<int>{}));

    // One stock price means you can buy it, but can't sell it, meaning no profit can be calculated
    EXPECT_EQ(0, getMaxProfit(std::vector<int>{10}));

    // Max stock price happens after min stock price.  Buy at $11, sell at $5, $6 profit
    EXPECT_EQ(6, getMaxProfit(std::vector<int>{10, 7, 5, 8, 11, 9}));

    // Max stock price happens before min stock price.  Buy at $9, sell at $5, $4 profit
    EXPECT_EQ(4, getMaxProfit(std::vector<int>{10, 11, 5, 8, 7, 9}));

    // Stock goes down during the entire range, which means profit is negative, but it still finds the smallest loss
    EXPECT_EQ(-1, getMaxProfit(std::vector<int>{10, 9, 2}));
}

int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}