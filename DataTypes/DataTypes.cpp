#include <forward_list>
#include <iostream>
#include <list>
#include <queue>
#include <stack>
#include <map>
#include <set>

int main()
{
    // Singly-linked list:
    std::forward_list<int> list1{ 1 ,2 ,3 };
    list1.emplace_front(4);

    // Doubly-linked list:
    std::list<int> list2{ 1, 1 };

    // Queue
    std::queue<int> myqueue;
    myqueue.push(1);

    // Stack
    std::stack<int> mystack;
    mystack.push(2);

    // Set
    std::set<int> setexample{ 10,10,20,30,40,50 };

    // Map (associative array?)
    std::map<std::string, int> first;
    first["whoooo"] = 10;
}
