// From Facebook coding assessment, July 2017
// Answer by: Steve Robinson

// Given weather temperatures in a region over last N days
// e.g.: 25, 30, 48, 15, 25, 45, 10, 25 write the function
// that returns the maximum temperature increase between
// any (not necessarily consecutive) 2 days.  If
// temperature remained constant or kept dropping,
// return 0.

// In the example, the temperature on day #4 was 15F and
// tempurature on day #6 was 45F, which means it increased
// by 30F over the course of those days - the maximum
// temperature increase between any 2 days.

// Note that the two temperatures are neither minimum
// (minimum temperature on day #7 was 10F), nor maximum
// (maximum temperature occurred on day #3 and was 48F).

// While there's an obvious O(N^2) solution, please
// provide O(N) solution.

#include "gtest/gtest.h"
#include <vector>

// O(N^2) algorithm
//int getMaxWarming(std::vector<int> temperatures)
//{
//    if (temperatures.size() < 2)
//    {
//        return 0;
//    }
//
//    int maxWarming = 0;
//
//    for (int i = 0; i < temperatures.size(); ++i)
//    {
//        int currentWarming = 0;
//        int maxTemp = temperatures.at(i);
//        for (int j = i+1; j < temperatures.size(); ++j)
//        {
//            int warming = temperatures.at(j) - maxTemp;
//            if (warming > 0)
//            {
//                currentWarming = currentWarming + warming;
//                maxTemp = temperatures.at(j);
//            }
//        }
//
//        if (currentWarming > maxWarming)
//            maxWarming = currentWarming;
//    }
//
//    return maxWarming;
//}

// O(N) algorithm
int getMaxWarming(std::vector<int> temperatures)
{
    if (temperatures.size() < 2)
    {
        return 0;
    }

    int maxWarming = 0;
    int currentWarming = 0;
    int maxTemp = temperatures.at(0);

    for (unsigned int i = 1; i < temperatures.size(); ++i)
    {
        int warming = temperatures.at(i) - maxTemp;
        if (warming > 0)
        {
            currentWarming = currentWarming + warming;
        }
        else
        {
            if (currentWarming > maxWarming)
            {
                maxWarming = currentWarming;
            }
            currentWarming = 0;
        }
        maxTemp = temperatures.at(i);
    }

    if (currentWarming > maxWarming)
    {
        maxWarming = currentWarming;
    }

    return maxWarming;
}

TEST(getMaxWarming, Tests)
{
    EXPECT_EQ(0, getMaxWarming(std::vector<int>{}));
    EXPECT_EQ(0, getMaxWarming(std::vector<int>{10, 10, 10, 10, 10}));
    EXPECT_EQ(0, getMaxWarming(std::vector<int>{50, 40, 30, 20, 10}));
    EXPECT_EQ(40, getMaxWarming(std::vector<int>{10, 20, 30, 40, 50}));
    EXPECT_EQ(44, getMaxWarming(std::vector<int>{23, 55, 67, 22, 40, 65, 44, 20}));
    EXPECT_EQ(30, getMaxWarming(std::vector<int>{25, 30, 48, 15, 25, 45, 10, 25}));
}

int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}