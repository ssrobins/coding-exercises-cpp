// You have a grid of letters like this:

// S E E
// S T O
// G E N

// Find all the combinations of words.

// Words can wrap up, down, left, right, but not diagonal

// Letters cannot be repeated for the same word, but they can be re - used for different words.In the above example, 'to' and 'tone' would be words.
// Assume you have a method that checks potential words against a dictionary.
// This question was asked during an in-person interview for the Amazon Silk Team

// Potential solutions: http://www.programcreek.com/2014/06/leetcode-word-search-ii-java/



// NOTE: Implementation is incomplete!!!

#include <vector>
#include "gtest/gtest.h"

bool matrixIsRectangular(std::vector<std::vector<char>> const& matrix)
{
    bool matrixIsRectangular = true;

    unsigned int secondDimension;
    for (unsigned int i = 0; i < matrix.size(); ++i)
    {
        if (i > 0 && secondDimension != matrix.at(i).size())
        {
            matrixIsRectangular = false;
            break;
        }
        
        secondDimension = matrix.at(i).size();
    }
    return matrixIsRectangular;
}

void wordSearch()
{
    // Sample word search board:
    std::vector<std::vector<char>> board{
        {'S', 'E', 'E'},
        {'S', 'T', 'O'},
        {'G', 'E', 'N'}
    };

    // This simulates a dictionary that contains by containing a list
    // of words present in the sample word search board.
    // In actuality, it should be a real dictionary of words
    std::vector<std::string> wordDictionary{
        "SEE",
        "SET",
        "TO",
        "TONE",
        "STONE",
        "GET"
    };

    // Loop through each letter on the board
    if (matrixIsRectangular(board))
    {
        unsigned int numRows = board.size();
        unsigned int numCols = board.at(0).size();
        for (unsigned int col = 0; col < numCols; ++col)
        {
            char character;
            for (unsigned int row = 0; row < numRows; ++row)
            {
                character = board.at(row).at(col);
            }
        }
    }
}

TEST(matrixIsSquare, Tests)
{
    EXPECT_TRUE(matrixIsRectangular(std::vector<std::vector<char>>{}));
    EXPECT_FALSE(matrixIsRectangular(std::vector<std::vector<char>>{{'A'}, {'B', 'C'}}));
    EXPECT_TRUE(matrixIsRectangular(std::vector<std::vector<char>>{{'A', 'B', 'C'}, {'D', 'E', 'F'}}));
}

int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}