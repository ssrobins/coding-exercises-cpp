#include <cmath>
#include <cstdio>
#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#include <sstream>
#include "gtest/gtest.h"
using namespace std;

class Time
{
public:
    Time()
    {
        set("00:00:00AM");
    }
    Time(string const& input)
    {
        set(input);
    }

    // Assumes string is of the form hh:mm:ssAM or hh:mm:ssPM
    Time set(string const& input)
    {
        string AmPm = input.substr(8, 2);
        int hoursAMPM = stoi(input.substr(0, 2));
        int hours24 = hoursAMPM;
        if (AmPm == "PM" && hoursAMPM != 12)
            hours24 = hoursAMPM % 12 + 12;
        else if (AmPm == "AM" && hoursAMPM == 12)
            hours24 = 0;

        stringstream stream;
        stream << hours24;
        hours = stream.str();

        if (hours24 < 10)
            hours.insert(0, "0");

        minutes = input.substr(3, 2);
        seconds = input.substr(6, 2);

        return *this;
    }

    string twentyFourHourFormat()
    {
        stringstream timeIn24hrFormat;
        timeIn24hrFormat << hours << ":" << minutes << ":" << seconds;
        return timeIn24hrFormat.str();
    }

    ~Time()
    {}

private:
    string hours; // 24-hour format
    string minutes;
    string seconds;
};

TEST(twentyFourHourFormat, Tests)
{
    EXPECT_EQ("00:00:00", Time("12:00:00AM").twentyFourHourFormat());
    EXPECT_EQ("12:00:00", Time("12:00:00PM").twentyFourHourFormat());
    EXPECT_EQ("01:10:20", Time("01:10:20AM").twentyFourHourFormat());
    EXPECT_EQ("13:00:00", Time("01:00:00PM").twentyFourHourFormat());
    EXPECT_EQ("18:00:00", Time("06:00:00PM").twentyFourHourFormat());
}

int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}